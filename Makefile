TIMESTAMP:=$(shell date +'%s')
DATE_FORMAT?=%a\n%d %B %Y\n%H:%M:%S
TEXT?=Hi @\n$(shell date +"$(DATE_FORMAT)" -d@$(TIMESTAMP))

%.png:
	convert -size 2400x1200 xc:White \
	  -gravity Center \
	  -weight 700 -pointsize 200 \
	  -annotate 0 "$(TEXT)" -channel RGBA -blur 0x6 \
	  -fill darkred -stroke magenta -strokewidth 3 \
	  -annotate 0 "$(TEXT)" \
	  $@

timestamp.txt:
	echo $(TIMESTAMP) > $@

clean:
	@rm -f now.png timestamp.txt

.PHONY: clean
