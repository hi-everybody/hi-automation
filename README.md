# Hi Automation

Use this repo to generate some repository activity, automatically.

## CI

Set the `GITLAB_TOKEN` environment variable for CI. It will be used to
push commits and write to the Wiki.
